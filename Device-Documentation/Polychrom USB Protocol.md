[[_TOC_]]

# Polychrome nuc121ZC2

The bulk of this  work was created by  https://github.com/aedalzottO.  And documented [here](https://github.com/aedalzotto/asrrgb/blob/master/docs/nuc121z1c.md).  This document is an expansion of his original reverse engineering work.

Reverse engineering of the protocol used for Polychrome Sync in ASRock B550 and TRX40 boards

## Information

The ASRock B550 and TRX40 boards moved the SMBus connected microcontroller to a Nuvoton NUC121ZC2 ARM microcontroller connected to USB.
This device is responsible for controlling the RGB illumination of the motherboard in up to 8 distinct regions, on/off mode plus 13 special effects independent for each zone.
zone
The NUC121 is ARMv6 M0 Cortex.  The Steel Legend APROM_MB_201104_b234_6c.bin (_Motherboard_Datecode_chksum_??).  Ghidra is able to decompile it. 

Write to endpoint 2, read from endpoint 1.

## Configurable fields

### Available commands
| Code (hex) |    COMMAND   |
|:----------:|:------------|
| 0x10     |     WRITE    |
| 0x11     |     READ     |
| 0x12     |    COMMIT    |
| 0x14     |  READ HEADER |
| 0x15     | WRITE HEADER |
| 0xA4     | INITIIALIZE  |



### Region identification
| Code (hex) |         REGION         |  Max LED Counts |
|:----------:|:----------------------|-:|
| 0x00     |      RGB Header 1      | 1 |
| 0x01     |      RGB Header 2      | 1 |
| 0x02     |  Addressable Header 1  | 100 |
| 0x03     |  Addressable Header 2  | 100 |
| 0x04     |      PCH Heatsink      | 5 |
| 0x05     |        I/O Cover       | 7 |
| 0x06     | PCB back / Audio Cover | ? |
| 0x07     | Audio Cover / PCB back | ? |

It is possible to set all regions at once setting the field ALL to 1. In this case, the region field will be ignored.
For more details, check the ALL field identification below.

### Mode identification
| Code (hex) |    MODE   |
|:----------:|:---------|
| 0x00     |    Off    |
| 0x01     |   Static  |
| 0x02     | Breathing |
| 0x03     |  Strobe   |
| 0x04     |  Cycling  |
| 0x05     |  Random   |
| 0x06     |   Music   |
| 0x07     |   Wave    |
| 0x08     |  Spring   |
| 0x09     |   Stack   |
| 0x0A     |   Cram    |
| 0x0B     |   Scan    |
| 0x0C     |   Neon    |
| 0x0D     |   Water   |
| 0x0E     |  Rainbow  |

Modes Off, Cycling, Random, Wave, Water and Rainbow will ignore the color configuration.
Modes Off, Static and Music will ignore the speed configuration.

The Music mode is a special one, where the software needs to send the colors accordingly to the music being played. This is not yet identified.

### Color identification
Each color is represented by 1 byte of R, G and B.

### Speed identification
The speed of the special effects is controlled by 1 byte. Maximum speed is set by 0, and minimum speed is set by 255 (0xFF).

### ALL identification
It is possible to write to all regions in a single command setting a byte to 1. This way, the region field will be ignored and the same mode, color and speed will be applied to all 7 regions. To manipulate regions individually, set this field to 0.

### Header options
The addressable header command has some options, with the expected reply size as follows:
| Code (hex) |    OPTION    |  CONFIG BYTES |
|:----------:|:------------|--------:|
| 0x01     | UNIDENTIFIED |  1 |
| 0x02     | LEDCOUNT CFG |  8 |
| 0x03     |  RGSWAP CFG  |  1 |

#### RG Swap Configuration ( 03)
The RGBWSAP CFG uses a 1 Byte to save what RGB Swapping is required.  It has 1 Bit to indicate the start, followed by a boolean for each available port.  They are in the reverse order of the regions value.  Unclear what happens on boards with 8 zones.


| AUDIO |  PCB | IO C | PCH | ARGB2 | ARGB1 | RGB2 | RGB1 |
|---|---|---|---|---|---|---|---|


Steel Legend (6 LED zones) 
|  0| 1 (LEADING 1) | IO C | PCH | ARGB2 | ARGB1 | RGB2 | RGB1 |
|---|---|---|---|---|---|---|---|

B550M -ITX/AC (4 LED zones) 
| 0 | 0 | 0 | 1 (LEADING1) | ARGB2 | ARGB1 | RGB2 | RGB1 |
|---|---|---|---|---|---|---|---|


#### Addressable LED Count Configuration (02)
This option gathers the the max LEDs for each zone.  It uses the following 8 Byte format.  No board with PCB or Audio have been tested to date to confirm their max setting.

Max Counts

| RGB1 | RGB2 | ARGB1 | ARGB2 | IO COVER | PCH | PCB | AUDIO | 
|:------:|:--:|:-----:|:---------|-------:|:------:|:------:|:-------------:|
| 01| 01 | 100 | 100 | 5 | 7 | ? | ? |

If any field is not available for that particular board it is set as 0x1E (30).  No confirmation as to what happens if a configuration with 30 Leds is saved to the header.

####  OPTION 1 (0x01)

The purposes of this option and its reply is unknown.  Different responses are seen for different boards.  No header write has been observed to this option

Board | CONFIG
--|--
B550 Steel Legend | 0x3F
B550- ITX/AC | 0x05



### Other fields

#### Second command byte
This byte is always 0x00.

#### READ/WRITE second-least byte
This byte is always 0xFF in READ and WRITE modes.

#### 0xA4 command
This commands are sent on Polychrome Sync software initialization. All of it's fields are unidentified.  This configures the Asrock to reply with A6 leading.  The first Answer Byte in all response varies depending on what state the controller is.

## Command details
All commands must have 64 bytes. The padding is made by zeroed bytes.

### Read  (0x11)
This command reads the value of the specific region requested.

| 0x11 (READ) | 0x00 | REGION |
|:---------:|:--:|:------:|

The device answers with the region confirmation, the selected mode, colors, speed and if all regions are synchronized.

| 0xA6 (ANSWER) | 0x00 | 0x00 | REGION | MODE | R | G | B | SPEED | 0xFF | ALL |
|:-----------:|:--:|:---|---:|:----:|:-:|:-:|:-:|:-----:|:--:|:---:|

### Write  (0x10)
This command writes the R, G, B, colors with the MODE selected at SPEED, optionally syncing all other regions with the field ALL.
In case the mode is Off, Static and Music, the speed will be ignored.
In case the mode is Off, Cycling, Random, Wave, Water and Rainbow, the colors will be ignored.

| 0x10 (WRITE) | 0x00 | REGION | MODE | R | G | B | SPEED | 0xFF | ALL |
|:----------:|:--:|:------:|:----:|:-:|:-:|:-:|:-----:|:--:|:---:|

The device sends a confirmation

| 0xA6 (ANSWER) | 0x00 | 0x00 (?) | 0x07 (OK?) |
|:-----------:|:--:|:------:|:--------:|

### Read header (0x14)
This command reads the addressable and non-addressable headers configurations:

| 0x14 (READ HDR) | 0x00 | OTPTION |
|:-------------:|:--:|:------:|

The device sends the answer still unknown.

| 0xA6 (ANSWER) | 0x00 | 0x00 (?) | OPTION| CONFIGURATION | 
|:-----------:|:--:|:------:|:------:|:------:|


### Write header (0x15)


The device answers in the following format:

| 0x15 (WRITE HDR) | 0x00 | 0x00  | OPTION | CONFIGURATION |
|:-----------:|:--:|:------:|:--------:|--

Upon a successful write the controller sends the following reply:

| 0xA6 (ANSWER) | 0x00 | 0x00 (?) | 0x07 (OK?) |
|:-----------:|:--:|:------:|:--------:|


### COMMIT (0x12)

On Polychrome Sync exit, it sends the device a command:

| 0x12 (commit) |
|:---------:|

If this command is not sent, all settings are not save to NVM.  This implies that the default state of all commands is direct.

### Command 0xA4
This is not yet identified. This is the first command to run when the Polychrome Sync software is initialized.

| 0xA4 (?) | 0x00 | 0x00 | 0x00 (?) | 0x01 (?) | 0x00 (?) | 0x00 (?) | 0x00 (?) | 0x01 (?) |
|:------:|:--:|:------:|:------:|:------:|:------:|:------:|:------:|-|

The answer received in the ASRock B550 Steel Legend with Polychrome Sync v2.62 and  Polychrome Sync v2.66 (beta), after suffering a firmware update is:


| 0xA6 (ANSWER) | 0x00 | 0x00 (?) | 0x00 (?) | 0x02 (?) |
|:-----------:|:--:|:------:|:------:|:------:|

So this probably isn't the firmware version.

## OTHER REPLIES

Sometimes the device answers like this:

| 0xA6 (ANSWER) | 0x00 | 0x00 (?) | 0x07 (?) | 0x02 (?) |
|:-----------:|:--:|:------:|:------:|:------:|

And it triggers a whole new type of packets, in the end resulting in a ABORT_PIPE, and making the Polychrome Sync software querying the device again.

## The Music protocol
In this mode, the Polychrome Sync software reads the sound output of the operating system and adds a byte with the corresponding intensity calculated by the program with an additional byte to the WRITE command. This is sent every 50\~60ms (16.66\~20Hz).

| 0x10 (WRITE) | 0x00 | REGION | 0x06 (MUSIC) | xx | xx | xx | xx | 0xFF | ALL | INTENSITY |
|:----------:|:--:|:------:|:----------:|:--:|:--:|:--:|:--:|:--:|:---:|:---------:|

The device sends a confirmation

| 0xA6 (ANSWER) | 0x00 | 0x00 (?) | 0x07 (OK?) |
|:-----------:|:--:|:------:|:--------:|
