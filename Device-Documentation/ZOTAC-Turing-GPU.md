# ZOTAC Turing GPU

ZOTAC's line of GeForce RTX 2xxx series graphics cards include RGB lighting via an I2C controller at address 0x49 on the GPU I2C bus.  We were unable to determine a good method of probing the I2C device directly, as there are several I2C buses on the GPU but only one seems to work at address 0x49.  One method to test is to write at this adress, and if the I2C API returns with no error, we can assume it's the I2C bus we're looking for.

The protocole has been reverse engineered by spying I2C calls of the ZOTAC Firestorm utility.  The RGB options in this software are limited (for example, there is no brightness or speed control), maybe more commands exist on the device.  Also, at initialization the software seems to do a lot of I2C calls that return an error, so it's not always clear what those commands are supposed to do and if they have an effect at all.

## **Supported Devices**

Note: Vendor and Device ID represent the nVidia GPU chipset and will be common to all cards using that chipset, even non-ZOTAC cards.  The sub-vendor and sub-device IDs are ZOTAC specific.

| PCI Vendor ID | PCI Device ID | PCI Sub-Vendor ID | PCI Sub-Device ID | Device Name                                  |
| ------------- | ------------- | ----------------- | ----------------- | -------------------------------------------- |
| 0x10DE        | 0x1E84        | 0x19DA            | 0x7500            | ZOTAC GAMING GeForce RTX 2070 SUPER Twin Fan |

## **I2C Registers**

| Register Address | Register Description        |
| ---------------- | --------------------------- |
| 0xA0             | Color, Mode and Speed       |
| 0xF1             | RGB LED On/Off ? (not sure) |

## **Setting Color, Mode and Speed**

The color, mode and speed are set by writting an 8-bytes length I2C packet to the register 0xA0:

| Byte index | Byte description                                          | Values                                               |
| ---------- | --------------------------------------------------------- | ---------------------------------------------------- |
| 0          | Register address                                          | 0xA0                                                 |
| 1          | Not really sure, but looks like some zone index (0 or 1). | 0x00 - 0x01                                          |
| 2          | Mode                                                      | 0x00 - 0x04                                          |
| 3          | Color's Red value                                         | 0x00 - 0xFF                                          |
| 4          | Color's Green value                                       | 0x00 - 0xFF                                          |
| 5          | Color's Blue value                                        | 0x00 - 0xFF                                          |
| 6          | Unknown                                                   | 0x00                                                 |
| 7          | Speed                                                     | Fastest: 0x00 - Slowest: 0x09 (not sure, maybe more) |

Note: Writing sequentially several I2C packets seems to hang the device (maybe a timing issue?).  As a workaround, reading the register after a write operation seems to avoid the next write operation to hang the RGB controller.

Note: on Linux, the register can be written with the following shell command (assuming I2C bus is /dev/i2c-2):

    i2cset 2 0x49 0xA0 0x00 0x03 0x00 0x00 0xFF 0x00 0x04 i

This sets the RGB controller to breathing mode (0x03), blue color (0x00 0x00 0xFF), intermediate speed (0x04).

### **Modes**

| Mode Value | Mode Description |
| ---------- | ---------------- |
| 0x00       | Static           |
| 0x01       | Strobe           |
| 0x02       | Rainbow Wave     |
| 0x03       | Breathing        |
| 0x04       | Spectrum Cycle   |

## **Reading Color, Mode and Speed**

Reading the 32 bytes of the 0xA0 register address returns the color, mode, speed and other undefined data:

| Byte index | Byte description             |
| ---------- | ---------------------------- |
| 0          | Mode (zone 0)                |
| 1          | Color's Red value (zone 0)   |
| 2          | Color's Green value (zone 0) |
| 3          | Color's Blue value (zone 0)  |
| 4          | Unknown (zone 0)             |
| 5          | Speed (zone 0)               |
| 6          | Mode (zone 1)                |
| 7          | Color's Red value (zone 1)   |
| 8          | Color's Green value (zone 1) |
| 9          | Color's Blue value (zone 1)  |
| 10         | Unknown (zone 1)             |
| 11         | Speed (zone 1)               |
| 12-31      | Unknown                      |

Note: on Linux, the register can be read with the following shell command (assuming I2C bus is /dev/i2c-2):

    i2cget 2 0x49 0xA0 i 32

